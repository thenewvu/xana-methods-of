'use strict';

/**
 * Return an array of method names of a service.
 * @param {Service} service
 * @returns {Array.<String>}
 * @private
 */
function methodsOfService(service) {
	const methods = [];
	let base      = Object.getPrototypeOf(service);
	let next_base = Object.getPrototypeOf(base);
	while (base && next_base) {
		Object.getOwnPropertyNames(base).forEach(prop => {
			// @formatter:off
			if (typeof service[prop] == 'function' &&
					prop !== 'constructor' &&
					!methods.includes(prop))
			{
				methods.push(prop);
			}
			// @formatter:on
		});

		base      = next_base;
		next_base = Object.getPrototypeOf(base);
	}
	return methods;
}

module.exports = methodsOfService;
