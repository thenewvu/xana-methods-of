'use strict';

const chai    = require('chai');
const decache = require('decache');
const expect  = chai.expect;

it('should list all methods of a service', function () {
	decache('../src/index.js');
	const methodsOfService = require('../src/index.js');

	class BaseService {
		constructor() {}

		get baseGetter() {}

		set baseSetter(value) {}

		baseMethod1() {}

		baseMethod2() {}
	}

	class DerivedService extends BaseService {
		constructor() {super();}

		get getter() {}

		set setter(value) {}

		baseMethod1() {}

		method1() {}

		method2() {}
	}

	const testService = new DerivedService();
	expect(methodsOfService(testService)).to.deep.equal([
		'baseMethod1',
		'method1',
		'method2',
		'baseMethod2'
	]);
});
